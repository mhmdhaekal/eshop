package id.ac.ui.cs.advprog.eshop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import id.ac.ui.cs.advprog.eshop.model.Product;
import id.ac.ui.cs.advprog.eshop.service.ProductService;


import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

  private final ProductService service;
  private final String REDIRECT_LIST = "redirect:list";

  public ProductController(ProductService service) {
      this.service = service;
  }

    @GetMapping("/create")
  public String createProductPage(Model model) {
    Product product = new Product();
    model.addAttribute("product", product);
    return "create-product";
  }

  @PostMapping("/create")
  public String createProductPost(@ModelAttribute Product product, Model model) {
    service.create(product);
    return REDIRECT_LIST;
  }

  @GetMapping("/list")
  public String productListPage(Model model) {
    List<Product> allProduct = service.findAll();
    model.addAttribute("products", allProduct);
    return "product-list";
  }

  @PostMapping("/delete")
  public String deleteProductPost(@ModelAttribute Product product, Model model) {
    service.delete(product.getProductId());
    return REDIRECT_LIST;
  }

  @GetMapping("/edit/{productId}")
  public String editProductPage(@PathVariable("productId") String productId, Model model) {
    Product product = service.findById(productId);

    if (product == null) {
      return "redirect:/product/list";
    }
    model.addAttribute("product", product);
      return "edit-product";
  }

  @PostMapping("/edit")
  public String editProductPost(@ModelAttribute Product product, Model model) {
    service.edit(product);
    return REDIRECT_LIST;
  }
}

