package id.ac.ui.cs.advprog.eshop.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import id.ac.ui.cs.advprog.eshop.model.Product;
import id.ac.ui.cs.advprog.eshop.repository.IProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

  private final IProductRepository productRepository;

    public ProductServiceImpl(IProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
  public Product create(Product product) {
    return productRepository.create(product);
  }

  @Override
  public List<Product> findAll() {
    Iterator<Product> productIterator = productRepository.findAll();
    List<Product> allProduct = new ArrayList<>();
    productIterator.forEachRemaining(allProduct::add);
    return allProduct;
  }

  @Override
  public Product delete(String id) {
    return productRepository.delete(id);
  }

  public Product edit(Product product) {
    productRepository.edit(product);
    return product;
  }

  public Product findById(String productId) {
    return productRepository.findById(productId);
  }

}
