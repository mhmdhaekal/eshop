package id.ac.ui.cs.advprog.eshop.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Car {
  private String carId;
  private String carName;
  private String carColor;
  private int carQuantity;

  public String getCarId() {
    return carId;
  }

  public String getCarName() {
    return carName;
  }

  public String getCarColor() {
    return carColor;
  }

  public int getCarQuantity() {
    return carQuantity;
  }

  public void setCarId(String carId){
    this.carId = carId;
  }

  public void setCarName(String carName){
    this.carName = carName;
  }

  public void setCarColor(String carColor){
    this.carColor = carColor;
  }
  public void setCarQuantity(int carQuantity){
    this.carQuantity = carQuantity;
  }
}
