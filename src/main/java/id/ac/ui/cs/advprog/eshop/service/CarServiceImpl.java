package id.ac.ui.cs.advprog.eshop.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import id.ac.ui.cs.advprog.eshop.model.Car;
import id.ac.ui.cs.advprog.eshop.repository.ICarRepository;

@Service
public class CarServiceImpl implements CarService{

    private final ICarRepository carRepository;

    public CarServiceImpl(ICarRepository carRepository){
        this.carRepository = carRepository;
    }

    @Override
    public Car create(Car car) {
        carRepository.create(car);
        return car;
    }

    @Override
    public List<Car> findAll(){
        Iterator<Car> carIterator = carRepository.findAll();
        List<Car> allCar = new ArrayList<>();
        carIterator.forEachRemaining(allCar::add);
        return allCar;        
    }

    @Override
    public Car findById(String id){
        return carRepository.findById(id);
    }

    @Override
    public void update(String id, Car updatedCar){
        carRepository.update(id, updatedCar);
    }

    @Override
    public void deleteCarById(String id){
        carRepository.delete(id);
    }    
}
