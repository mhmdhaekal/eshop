package id.ac.ui.cs.advprog.eshop.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Repository;

import id.ac.ui.cs.advprog.eshop.model.Product;

@Repository
public class ProductRepository implements IProductRepository{
  private List<Product> productData = new ArrayList<>();
  private int productCount = 1;

  public Product create(Product product) {
    product.setProductId("P" + productCount++);
    productData.add(product);
    return product;
  }

  public Product delete(String id) {
    for (Product p : productData) {
      if (p.getProductId().equals(id)) {
        productData.remove(p);
        return p;
      }
    }
    return null;
  }

  public Iterator<Product> findAll() {
    return productData.iterator();
  }

  public Product edit(Product product) {
    for (Product p : productData) {
      if (p.getProductId().equals(product.getProductId())) {
        p.setProductName(product.getProductName());
        p.setProductQuantity(product.getProductQuantity());
        return p;
      }
    }
    return null;
  }

  public Product findById(String productId) {
    for (Product p : productData) {
      if (p.getProductId().equals(productId)) {
        return p;
      }
    }
    return null;
  }

}
