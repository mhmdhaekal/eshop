package id.ac.ui.cs.advprog.eshop.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import id.ac.ui.cs.advprog.eshop.model.Car;
import id.ac.ui.cs.advprog.eshop.service.CarService;


@Controller
@RequestMapping("/car")
public class CarController {
  private final CarService carService;
  private final String REDIRECT_LIST = "redirect:list";

  public CarController(CarService carService) {
    this.carService = carService;
  }

  @GetMapping("/create")
  public String createCarPage(Model model) {
    Car car = new Car();
    model.addAttribute("car", car);
    return "create-car";
 
  }

  @PostMapping("/create")
  public String createCarPost(@ModelAttribute Car car, Model model) {
    carService.create(car);
    return REDIRECT_LIST;
  }

  @GetMapping("/list")
  public String carListPage(Model model) {
    List<Car> allCar = carService.findAll();
    model.addAttribute("cars", allCar);
    return "car-list";
  }

  @GetMapping("/edit/{carId}")
  public String editCarPage(@PathVariable("carId") String carId, Model model) {
    Car car = carService.findById(carId);
    model.addAttribute("car", car);
    return "edit-car";
  }

  @PostMapping("/edit")
  public String editCarPost(@ModelAttribute Car car, Model model) {
    carService.update(car.getCarId(), car);
    return REDIRECT_LIST;
  }

  @PostMapping("/delete")
  public String deleteCarPost(@ModelAttribute Car car, Model model) {
    carService.deleteCarById(car.getCarId());
    return REDIRECT_LIST;
  }
}