package id.ac.ui.cs.advprog.eshop.repository;

import java.util.Iterator;

import id.ac.ui.cs.advprog.eshop.model.Product;

public interface IProductRepository {
    public Product create(Product product);
    public Product findById(String id);
    public Product edit(Product updatedProduct);
    public Product delete(String id);
    public Iterator<Product> findAll();    
} 
