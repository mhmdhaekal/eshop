package id.ac.ui.cs.advprog.eshop.service;

import java.util.List;

import id.ac.ui.cs.advprog.eshop.model.Car;

public interface CarService {

    public Car create(Car car);
    public List<Car> findAll();
    public Car findById(String id);
    public void update(String id, Car Car);
    public void deleteCarById(String id);

    
}
