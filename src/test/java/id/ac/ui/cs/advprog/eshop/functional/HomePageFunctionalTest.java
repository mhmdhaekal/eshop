package id.ac.ui.cs.advprog.eshop.functional;


import io.github.bonigarcia.seljup.SeleniumJupiter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ExtendWith(SeleniumJupiter.class)
 class HomePageFunctionalTest {
    @LocalServerPort
    private int serverPort;

    @Value("${app.baseUrl:http://localhost}")
    private String testBaseUrl;

    private String baseUrl;

    private ChromeDriver driver;

    @BeforeEach
    void setupTest(){
        baseUrl = String.format("%s:%d", testBaseUrl, serverPort);
        driver = new ChromeDriver();
    }

    @Test
    void welcomeMessage_homePage_isCorrect() throws Exception{
        driver.get(baseUrl);
        String welcomeMessage = driver.findElement(By.tagName("h1")).getText();
        assertEquals("Let's Shop", welcomeMessage);
        driver.quit();
    }

    @Test
    void menuButton_homePage_isCorrect() throws Exception{
        driver.get(baseUrl);
        boolean createButton = driver.findElement(By.id("create-product-button")).isDisplayed();
        boolean listButton = driver.findElement(By.id("list-product-button")).isDisplayed();
        assertTrue(createButton);
        assertTrue(listButton);
        driver.quit();
    }
}