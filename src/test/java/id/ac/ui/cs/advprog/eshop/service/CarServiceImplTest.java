package id.ac.ui.cs.advprog.eshop.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import id.ac.ui.cs.advprog.eshop.model.Car;
import id.ac.ui.cs.advprog.eshop.repository.ICarRepository;

public class CarServiceImplTest {

    @InjectMocks
    private CarServiceImpl carService;

    @Mock
    private ICarRepository carRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreate() {
        Car car = new Car();
        carService.create(car);
        verify(carRepository, times(1)).create(car);
    }

    @Test
    public void testFindAll() {
        Car car1 = new Car();
        Car car2 = new Car();
        List<Car> cars = Arrays.asList(car1, car2);
        when(carRepository.findAll()).thenReturn(cars.iterator());
        carService.findAll();
        verify(carRepository, times(1)).findAll();
    }

    @Test
    public void testFindById() {
        Car car = new Car();
        String id = "CAR123";
        when(carRepository.findById(id)).thenReturn(car);
        carService.findById(id);
        verify(carRepository, times(1)).findById(id);
    }

    @Test
    public void testUpdate() {
        Car car = new Car();
        String id = "CAR123";
        carService.update(id, car);
        verify(carRepository, times(1)).update(id, car);
    }

    @Test
    public void testDeleteCarById() {
        String id = "CAR123";
        carService.deleteCarById(id);
        verify(carRepository, times(1)).delete(id);
    }
}