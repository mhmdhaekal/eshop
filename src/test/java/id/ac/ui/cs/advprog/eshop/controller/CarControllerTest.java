package id.ac.ui.cs.advprog.eshop.controller;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import id.ac.ui.cs.advprog.eshop.model.Car;
import id.ac.ui.cs.advprog.eshop.service.CarService;

public class CarControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private CarController carController;

    @Mock
    private CarService carService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(carController).build();
    }

    @Test
    public void testCreateCarPage() throws Exception {
        mockMvc.perform(get("/car/create"))
            .andExpect(status().isOk())
            .andExpect(view().name("create-car"));
    }

    @Test
    public void testCreateCarPost() throws Exception {
        mockMvc.perform(post("/car/create"))
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:list"));
        verify(carService, times(1)).create(any(Car.class));
    }

    @Test
    public void testCarListPage() throws Exception {
        mockMvc.perform(get("/car/list"))
            .andExpect(status().isOk())
            .andExpect(view().name("car-list"));
    }

    @Test
    public void testEditCarPage() throws Exception {
        when(carService.findById(any(String.class))).thenReturn(new Car());
        mockMvc.perform(get("/car/edit/CAR123"))
            .andExpect(status().isOk())
            .andExpect(view().name("edit-car"));
    }

    @Test
    public void testEditCarPost() throws Exception {
        mockMvc.perform(post("/car/edit"))
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:list"));
    }

    @Test
    public void testDeleteCarPost() throws Exception {
        mockMvc.perform(post("/car/delete")
            .param("carId", "CAR123"))
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:list"));
    }
}