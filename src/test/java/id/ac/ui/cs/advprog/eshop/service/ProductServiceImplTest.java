package id.ac.ui.cs.advprog.eshop.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import id.ac.ui.cs.advprog.eshop.model.Product;
import id.ac.ui.cs.advprog.eshop.repository.IProductRepository;

public class ProductServiceImplTest {

    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private IProductRepository productRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreate() {
        Product product = new Product();
        productService.create(product);
        verify(productRepository, times(1)).create(product);
    }

    @Test
    public void testFindAll() {
        Product product1 = new Product();
        Product product2 = new Product();
        List<Product> products = Arrays.asList(product1, product2);
        when(productRepository.findAll()).thenReturn(products.iterator());
        productService.findAll();
        verify(productRepository, times(1)).findAll();
    }

    @Test
    public void testDelete() {
        String id = "PROD123";
        productService.delete(id);
        verify(productRepository, times(1)).delete(id);
    }

    @Test
    public void testEdit() {
        Product product = new Product();
        productService.edit(product);
        verify(productRepository, times(1)).edit(product);
    }

    @Test
    public void testFindById() {
        Product product = new Product();
        String id = "PROD123";
        when(productRepository.findById(id)).thenReturn(product);
        productService.findById(id);
        verify(productRepository, times(1)).findById(id);
    }
}