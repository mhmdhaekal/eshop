package id.ac.ui.cs.advprog.eshop.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CarTest {
    private Car car;

    @BeforeEach
    public void setUp() {
        car = new Car();
    }

    @Test
    public void testSetAndGetCarId() {
        UUID carId = UUID.randomUUID();
        car.setCarId(carId.toString());
        assertEquals(car.getCarId(), carId.toString());
    }

    @Test
    public void testSetAndGetCarName() {
        String carName = "Tesla Model S";
        car.setCarName(carName);
        assertEquals(car.getCarName(), carName);
    }

    @Test
    public void testSetAndGetCarColor() {
        String carColor = "Red";
        car.setCarColor(carColor);
        assertEquals(car.getCarColor(), carColor);
    }

    @Test
    public void testSetAndGetCarQuantity() {
        int carQuantity = 5;
        car.setCarQuantity(carQuantity);
        assertEquals(car.getCarQuantity(), carQuantity);
    }
}