package id.ac.ui.cs.advprog.eshop.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.eshop.model.Product;

@ExtendWith(MockitoExtension.class)
class ProductRepositoryTest {

  @InjectMocks
  ProductRepository productRepository;

  @BeforeEach
  void setup() {
  }

  @Test
  void testCreateAndFind() {
    Product product = new Product();
    product.setProductName("Sampo Cap Bambang");
    product.setProductQuantity(100);

    productRepository.create(product);

    Iterator<Product> productIterator = productRepository.findAll();

    assertTrue(productIterator.hasNext());

    Product savedProduct = productIterator.next();

    assertEquals(product.getProductId(), savedProduct.getProductId());
    assertEquals(product.getProductName(), savedProduct.getProductName());
    assertEquals(product.getProductQuantity(), savedProduct.getProductQuantity());
  }

  @Test
  void testFindAllIfEmpty() {
    Iterator<Product> productIterator = productRepository.findAll();
    assertFalse(productIterator.hasNext());
  }

  @Test
  void testFindAllMoreThanOneProduct() {
    Product product1 = new Product();
    product1.setProductName("Sampo Cap Bambang");
    product1.setProductQuantity(100);
    productRepository.create(product1);

    Product product2 = new Product();
    product2.setProductName("Sampo Cap Usep");
    product2.setProductQuantity(50);
    productRepository.create(product2);

    Iterator<Product> productIterator = productRepository.findAll();
    assertTrue(productIterator.hasNext());

    Product savedProduct = productIterator.next();
    assertEquals(product1.getProductId(), savedProduct.getProductId());
    savedProduct = productIterator.next();
    assertEquals(product2.getProductId(), savedProduct.getProductId());
    assertFalse(productIterator.hasNext());

  }

  @Test
  void testDelete() {
    Product product = new Product();
    product.setProductName("Sampo Cap Bambang");
    product.setProductQuantity(100);
    productRepository.create(product);

    Iterator<Product> productIterator = productRepository.findAll();
    assertTrue(productIterator.hasNext());

    Product deletedProduct = productRepository.delete(product.getProductId());
    assertEquals(product.getProductId(), deletedProduct.getProductId());
  }

  @Test
  void findById() {
    Product product = new Product();
    product.setProductName("Sampo Cap Bambang");
    product.setProductQuantity(100);
    productRepository.create(product);
    Product foundProduct = productRepository.findById(product.getProductId());
    assertEquals(product.getProductId(), foundProduct.getProductId());
  }

  void findByIdIfEmpty() {
    Product product = new Product();
    product.setProductName("Sampo Cap Bambang");
    product.setProductQuantity(100);
    Product foundProduct = productRepository.findById(product.getProductId());
    assertEquals(null, foundProduct);
  }

  void findByIdIfNotFound() {
    Product product1 = new Product();
    productRepository.create(product1);

    Product product2 = new Product();
    Product foundProduct = productRepository.findById(product2.getProductId());

    assertEquals(null, foundProduct);
  }

  @Test
  void editProduct() {
    Product product1 = new Product();
    product1.setProductName("Sampo Cap Bambang");
    product1.setProductQuantity(100);
    productRepository.create(product1);

    Product product2 = new Product();
    product2.setProductId(product1.getProductId());
    product2.setProductName("Sampo Cap Haekal");
    product2.setProductQuantity(10000);
    productRepository.edit(product2);
    assertEquals(product2.getProductId(), product1.getProductId());
    assertEquals(product2.getProductName(), product1.getProductName());
    assertEquals(product2.getProductQuantity(), product1.getProductQuantity());

  }
}
