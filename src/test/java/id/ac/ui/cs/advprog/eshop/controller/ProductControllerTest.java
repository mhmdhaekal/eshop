package id.ac.ui.cs.advprog.eshop.controller;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import id.ac.ui.cs.advprog.eshop.model.Product;
import id.ac.ui.cs.advprog.eshop.service.ProductService;

import java.util.Arrays;
import java.util.List;

public class ProductControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private ProductController productController;

    @Mock
    private ProductService productService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    public void testCreateProductPage() throws Exception {
        mockMvc.perform(get("/product/create"))
            .andExpect(status().isOk())
            .andExpect(view().name("create-product"));
    }

    @Test
    public void testCreateProductPost() throws Exception {
        mockMvc.perform(post("/product/create"))
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:list"));
        verify(productService, times(1)).create(any(Product.class));
    }

    @Test
    public void testProductListPage() throws Exception {
        Product product1 = new Product();
        Product product2 = new Product();
        List<Product> products = Arrays.asList(product1, product2);
        when(productService.findAll()).thenReturn(products);
        mockMvc.perform(get("/product/list"))
            .andExpect(status().isOk())
            .andExpect(view().name("product-list"));
    }

    @Test
    public void testDeleteProductPost() throws Exception {
        mockMvc.perform(post("/product/delete"))
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:list"));
    }

    @Test
    public void testEditProductPage() throws Exception {
        Product product = new Product();
        String id = "PROD123";
        when(productService.findById(id)).thenReturn(product);
        mockMvc.perform(get("/product/edit/" + id))
            .andExpect(status().isOk())
            .andExpect(view().name("edit-product"));
    }

    @Test
    public void testEditProductPost() throws Exception {
        mockMvc.perform(post("/product/edit"))
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:list"));
        verify(productService, times(1)).edit(any(Product.class));
    }
}