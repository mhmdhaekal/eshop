package id.ac.ui.cs.advprog.eshop;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import id.ac.ui.cs.advprog.eshop.controller.ProductController;
import id.ac.ui.cs.advprog.eshop.controller.CarController;

@SpringBootTest
class EshopApplicationTests {

	@InjectMocks
	private ProductController productController;


	@InjectMocks
	private CarController carController;
	

	@Test
	void contextLoads() {
		assertNotNull(productController);
		assertNotNull(carController);
	}

}
