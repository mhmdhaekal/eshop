package id.ac.ui.cs.advprog.eshop.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Iterator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.eshop.model.Car;

public class CarRepositoryTest {

    private CarRepository carRepository;

    @BeforeEach
    public void setup() {
        carRepository = new CarRepository();
    }

    @Test
    public void testCreateCar() {
        Car car = new Car();
        Car createdCar = carRepository.create(car);
        assertNotNull(createdCar.getCarId());
        assertEquals(car, createdCar);
    }

    @Test
    public void testFindAllCars() {
        Car car = new Car();
        carRepository.create(car);
        Iterator<Car> cars = carRepository.findAll();
        assertEquals(car, cars.next());
    }

    @Test
    public void testFindCarById() {
        Car car = new Car();
        Car createdCar = carRepository.create(car);
        Car foundCar = carRepository.findById(createdCar.getCarId());
        assertEquals(createdCar, foundCar);
    }

    @Test
    public void testUpdateCar() {
        Car car = new Car();
        Car createdCar = carRepository.create(car);
        Car updatedCar = new Car();
        updatedCar.setCarName("Updated Car");
        Car resultCar = carRepository.update(createdCar.getCarId(), updatedCar);
        assertEquals("Updated Car", resultCar.getCarName());
    }

    @Test
    public void testDeleteCar() {
        Car car = new Car();
        Car createdCar = carRepository.create(car);
        Car deletedCar = carRepository.delete(createdCar.getCarId());
        assertEquals(createdCar, deletedCar);
        assertNull(carRepository.findById(createdCar.getCarId()));
    }
}