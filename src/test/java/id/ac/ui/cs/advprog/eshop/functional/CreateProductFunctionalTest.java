package id.ac.ui.cs.advprog.eshop.functional;


import io.github.bonigarcia.seljup.SeleniumJupiter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SeleniumJupiter.class)
 class CreateProductFunctionalTest {
    @LocalServerPort
    private int serverPort;
    
    @Value("${app.baseUrl:http://localhost}")
    private String testBaseUrl;
    
    private String baseUrl;

    private ChromeDriver driver;
    
    @BeforeEach
    void setupTest(){
        baseUrl = String.format("%s:%d", testBaseUrl, serverPort);
        driver = new ChromeDriver();
    }
    
    @Test
    void createButton_productListPage_isRendered() throws  Exception{
        String url = String.format("%s/%s", baseUrl, "product/list");
        driver.get(url);
        boolean createButton =driver.findElement(By.id("create-button")).isDisplayed();
        assertTrue(createButton);
        driver.quit(); 
    }
    
   
    @Test
    void createProductFunction_isExpected() throws Exception{
        String url = String.format("%s/%s", baseUrl, "product/list");
        driver.get(url);
        
        int productSize = driver.findElements(By.id("product")).size();
        
        driver.findElement(By.id("create-button")).click();
        
        String current_url  = driver.getCurrentUrl();
        String expected_url = String.format("%s/%s", baseUrl, "product/create");
        assertEquals(current_url, expected_url);
        
        driver.findElement(By.id("nameInput")).sendKeys("Kecap Bango");
        driver.findElement(By.id("quantityInput")).sendKeys("10");
        driver.findElement(By.id("submit-button")).click();
        
        current_url = driver.getCurrentUrl();
        assertEquals(current_url, url);
        
        int updatedProductSize = driver.findElements(By.id("product")).size();
        assertEquals(productSize+1, updatedProductSize);
        driver.quit();
        
    }
    
}