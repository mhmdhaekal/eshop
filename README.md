[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=mhmdhaekal_eshop)

# eshop

url : [https://eshop-5bsr.onrender.com/](https://eshop-5bsr.onrender.com/)

**Muhammad Haekal Kalipaksi**

**2206817490**

## Exercise 1 Reflection

Pada minggu ini saya mempelajari mengenai coding standard dan secure coding. Coding standard adalah aturan yang digunakan untuk menulis kode program dengan baik dan benar, diharapkan dengan adanya coding standard code program dapat dimengerti dan memiliki tingkat maintainability yang baik. Sedangkan secure coding merupakan teknik penulisan kode program yang aman.

Pada module minggu ini saya mengimplementasikan coding standard sebagai berikut:

- Menggunakan nama variabel yang jelas dan mudah dimengerti.
- Membagi kode program sesuai dengan fungsinya, dalam konteks MVC saya membagi kode dengan:
  - Model: kode yang berhubungan dengan database dan data.
  - View: kode yang berhubungan dengan tampilan.
  - Controller: kode yang berhubungan dengan routing dari sistem.
  - Service: kode yang berhubungan dengan bisnis logic.
  - Repository: kode yang berhubungan dengan manipulasi model/data.
- Menggunakan gitflow dalam pengembangan kode program.
- Melakukan micro commit, yaitu commit yang dilakukan setiap kali ada perubahan kode program.
- Memberikan commit message yang jelas dan dapat dimengerti.
- Memastikan fungsionalitas kode program dengan membuat unit test.

Selain itu, saya juga mengimplementasikan secure coding sebagai berikut:

- Menggunakan GPG signature untuk commit, sehingga commit yang dilakukan dapat diverifikasi di gitlab.
- Memastikan kode program yang saya tulis tidak memiliki bug, dengan mengimplementasikan unit test.

Diharapkan dengan mempelajari materi dan module pada minggu ini saya dapat meningkatkan kualitas kode yang saya tulis. Tentunya saya ingin tetap meningkatkan kualitas kode saya pada module kali ini jika ada yang kurang sesuai dengan coding standard.

## Exercise 2 Reflection

Setelah menulis unit test, saya lebih yakin bahwa code yang saya tulis memiliki fungsionalitas sesuai dengan yang diharapkan. Untuk memastikan unit test cukup untuk memverifikasi program adalah dengan membuat seluruh case untuk fungsi (positive flow, negative flow, dan negative flow). Selai itu, test coverage juga dapat menjadi indikasi bahwa unit test yang dibuat baik, 100% test coverage bukan berarti memiliki kode yang tidak ada bug (mungkin saja test yang dibuat hanya positive flow dan tidak ada edge case), namun dengan 100% test coverage dapat memastikan seluruh code yang dibuat memiliki unit test. Dalam pertanyaan nomor dua code quality tentunya akan lebih rendah, dikarenakan code yan ditulis tidak jauh berbeda dengan fungsionalitas code CreateProductFunctinoalTest.java. Masalah yang akan terjadi adalah menulis dua code yang mirip dua kali, hal yang mungkin dilakukan adalah meninclude test number of items in product list setelah membuat product.

## Exercise 3 Reflection

**Nomor 1**

Dalam analisis dengan menggunakan sonarcloud saya mendapatkan 9 issues yang berkaitan dengan code quality, berikut adalah strategi saya dalam menyelesaikannya:

- Unused import (3 problem): Dalam permasalahan ini saya hanya menghapus depedencies import yang tidak digunakan
- Kesalahan pernamaan method (1 problem): Permasalahan ini disebabkan oleh saya menggunakan PascalCase dalam menamakan method, seharusnya dalam java menggunakan camelCase
- Penggunaan field injection (2 problem): Penggunaan field inejction tidak digunakan dikarenakan memungkinkan pembuatan objek dengan state yang tidak tentu dan membuat code susah dimengerti dan maintainability yang tidak baik. Oleh karena itu, saya mengubah field injection `@Autowired` menjadi constructor injection.
- Remove public modifier (2): modifier dalam JUNIT5 test seharusnya default, sehingga `public` modifier tidak dibutuhkan
- Remove duplication code (1): terdapat beberapa duplication code sehingga diubah menjadi constant.

Strategi saya dalam memperbaiki code tersebut adalah memahami permasalahan yang ada, lalu membaca rekomendasi yang diberikan oleh sonarcloud dan mengeimplementasikan di source code.

**Nomor 2**

Dalam implementasi CI/CD saya sudah membuat CI sebagai berikut:

- SAST: untuk static code analytics menggunakan templates dari gitlab
- code quality: untuk code quality analysis menggunakan templates dari gitlab
- gradle-test : digunakan untuk menjalankan test gradle
- scorecard: digunakan untuk menjanalankan OSSF scorecard
- soundcloudcheck: digunakan untuk menjalankan soundcloud

CD:

- Membuat `Dockerfile` dan membuat trigger di `Render` ketika terjadi push ke main akan `auto deploy`

Berdasarkan CI yang saya tulis diatas, menurut saya sudah memenuhi implementasi CI, hal tersebut dikarenakan CI yang saya buat mengurangi task yang repetitive dan membuatnya menjadi sebuah automasi. Contohnya adalah ketika saya melakukan push/merge request seluruh CI diatas akan berjalan dan memberikan report sehingga jika terdapat pull request yang tidak pass test, dapat cepat diketahui. Untuk CD, saya sudah membuat Dockerfile dan menggunakan render untuk deployment, sehingga ketika push ke main, render akan deploy secara otomatis.

## Exercise 4

- Berdasarkan kode yang saya tulis, saya sudah mengimplementasikan _Single Responbility Principle_, sehingga dalam codebase saya suatu class hanya memiliki satu tanggung jawab dan fungsi.
- Berdasarkan kode yang saya tulis, saya sudah mengimplementasikan _Open/Closed Principle_, sehingga dalam codebase saya suatu class dapat extensible tanpa harus memodifikasi class tersebut.
- Berdasarkan kode yang saya tulis, saya sudah mengimplementasikan _Liskov Subsitution Principle_, sehingga dalam codebase saya tidak ada class yang tidak berkaitan memiliki suatu subclass.
- Berdasarkan kode yang saya tulis, saya sudah mengimplementasikan _Interface Segregation Principle_, sehingga dalam codebase saya service dan repository memiliki interface.
- Berdasarkan kode yang saya tulis, saya sudah mengimplementasikan _Dependency Inversion Principle_, sehingga dalam codebase saya tidak ada penggunaan low level modules secara langsung di high level modules.

## Exercise 4 Reflection

**Nomor 1**

Berikut ada principle yang saya terapkan dalam proyek saya:

- _Single Responbility Principle_, dalam codebase ini saya memastikan suatu class hanya memiliki satu tanggung jawab dan fungsi, contohnya adalah implementasi CarController.java yang di `before-solid` merupakan subclass dari `ProductController.java`
- _Open/Closed Principle_, dalam codebase ini belum module yang saya buat dapat extensible tanpa harus mengubah suatu class.
- _Liskov Subtitution Principle_, contohnya dalam codebase ini adalah CarController.java yang menjadi suatu class sendiri, dikarenakan jika merupakan subclass dari `ProductController.java` endpoint `/car/list` menjadi sama dengan `product/list`, hal tersebut dapat diatasi dengan membuat CarController menjadi class.
- _Interface Segregation Principle_, contohnya dalam codebase ini, service dan repository memiliki interface.
- _Dependency Inversion Principle_, contohnya dalam codebase ini, high level modele tidak ada yang langsung menggunaka low level module, harus melalui interface.

**Nomor 2**

Manfaat dari SOLID principle adalah sebagai berikut:

- Codebase memiliki maintainbility yang tinggi, ketika menerapkan `Single Responbility Principle` suatu class hanya memiliki satu fungsi, sehingga orang lain dapat mengerti fungsi dan responsibility dari suatu class. Contohnya adalah dalam memisahkan `CarController.java` dan `ProductController.java` kedua class tersebut memiliki fungsi dan endpoint controller yang berbeda.
- Codebase dapat lebih mudah di test, ketika menerapkan `SRP` dan `ISP` testing akan mudah dilakukan dikarenakan suatu modul tidak terlalu besar dan fungsinya jelas. Selain itu ketika menggunakan interface, seluruh method harus diimplementasi dan fungsinya jelas.
- Menghindari bug, ketika mengimplementasikan `LSP` bug yang disebabkan oleh extend yang tidak seharusnya dapat dihindari. Contohnya adalah bug yang terjadi di `before-solid` yaitu endpoint `/car/list` akan sama dengan `/product/list` seharusnya tidak.

**Nomor 3**

Kerugian ketika tidak mengimplemntasikan SOLID principle adalah sebagai berikut:

- Codebase menjadi berantakan, ketika tidak mengimplementasikan `SRP` suatu class dapat memiliki berbagai responsibility, sehingga sulit untuk memiliki `maintainibility` yang rendah dan sulit untuk dipahami. Contohnya adalah code CarController.java yang ada di ProductController.java di `before-solid`, seharusnya kedua class tersebut adalah class yang memiliki endpoint dan fungsi yang berbeda.
- Membuat unit test sulit, ketika tidak mengimplementasikan `SRP`, `LSP`, dan `ISP`, dalam membuat test case harus menghandle banyak case untuk seluruh tanggung jawab dari suatu class, selain itu bisa saja bug yang disebabkan oleh `LSP` tidak `dihandle`. Contohnya, dalam codebase saat ini, jumlah unit test meningkat.
- Bug akan terjadi, ketika `LSP` tidak diterapkan suatu subclass yang seharusnya memiliki fungsi beda memiliki fungsi yang tetap diwarisi dari class. Contohnya adalah bug yang terjadi di `before-solid` yaitu endpoint `/car/list` akan sama dengan `/product/list` seharusnya tidak.
